#pragma once

/*
 *  polyclib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "geom.h"
#include "integer.h"

#include <array>
#include <vector>
#include <iterator>
#include <optional>
#include <algorithm>

#include <cassert>

namespace polyclib {
  // util?
  template <typename Enum>
  void enumSet(Enum& set, Enum flag) {
    using U = std::underlying_type_t<Enum>;
    set = static_cast<Enum>(static_cast<U>(set) | static_cast<U>(flag));
  }
  template <typename Enum>
  void enumReset(Enum& set, Enum flag) {
    using U = std::underlying_type_t<Enum>;
    set = static_cast<Enum>(static_cast<U>(set) & static_cast<U>(~static_cast<U>(flag)));
  }
  template <typename Enum>
  bool enumTest(Enum set, Enum flag) {
    using U = std::underlying_type_t<Enum>;
    return !!(static_cast<U>(set) & static_cast<U>(flag));
  }
  template <typename Enum>
  constexpr Enum enumAnd(Enum l, Enum r) {
    using U = std::underlying_type_t<Enum>;
    return static_cast<Enum>(static_cast<U>(l) & static_cast<U>(r));
  }
  template <typename Enum>
  constexpr Enum enumOr(Enum l, Enum r) {
    using U = std::underlying_type_t<Enum>;
    return static_cast<Enum>(static_cast<U>(l) | static_cast<U>(r));
  }
  template <typename Enum>
  constexpr Enum enumSetAll() {
    using U = std::underlying_type_t<Enum>;
    return static_cast<Enum>(static_cast<U>(~static_cast<U>(0)));
  }
  template <typename Enum>
  constexpr Enum enumResetAll() {
    using U = std::underlying_type_t<Enum>;
    return static_cast<Enum>(static_cast<U>(0));
  }

  template <typename GC>
  class Graph {
    public:
      using Geom = GC;
      using Point = typename Geom::Point;
      using PEdge = typename Geom::Edge;

      enum class NTp : std::uint16_t {
        subject = 1u<<0,
        clip    = 1u<<1,
        cut     = 1u<<2,
        corner  = 1u<<3,
      };
      enum class ETp : std::uint16_t {
        subject = 1u<<0,
        clip    = 1u<<1,
      };

      struct NodeIdTag {};
      struct EdgeIdTag {};
      using NodeId = util::SafeInt<std::size_t,NodeIdTag>;
      using EdgeId = util::SafeInt<std::size_t,EdgeIdTag>;

      struct Edge {
        friend Graph;
        private:
          EdgeId const id;
          std::array<NodeId,2> const nodes;
          Edge(Edge const&) = delete;
          Edge& operator=(Edge const&) = delete;
          Edge& operator=(Edge&&) = delete;
        public:
          Edge(Edge&&) = default;
          Edge(EdgeId id, NodeId n0, NodeId n1, PEdge pedge, ETp flags)
            : id(id), nodes{n0,n1}, pedge(pedge), flags(flags) {}
          PEdge const pedge;
          ETp flags;
          NodeId operator[](std::size_t k) const {
            return nodes[k];
          }
      };
      struct Node {
        friend Graph;
        private:
          NodeId const id;
          // candidate for flacl Vector
          std::vector<EdgeId> adj;
          Node(Node const&) = delete;
          Node& operator=(Node const&) = delete;
          Node& operator=(Node&&) = delete;
        public:
          #pragma GCC diagnostic push
          #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
          // no idea. gcc bug?
          Node(Node&&) = default;
          #pragma GCC diagnostic pop
          //: id(std::move(from.id)), adj(std::move(from.adj)), point(std::move(from.point)), flags(std::move(from.flags)) {}
          Node(NodeId id, Point point, NTp flags) : id(id), adj(), point(point), flags(flags) {}
          Point const point;
          NTp flags;
      };

    private:

      // zero is always nullopt
      std::vector<std::optional<Node>> nodes;
      std::vector<std::optional<Edge>> edges;

    public:

      static constexpr NodeId invalidN = NodeId(0);
      static constexpr EdgeId invalidE = EdgeId(0);

      inline std::optional<Node>& ato(NodeId const nid) noexcept {
        assert(nid.get() < nodes.size());
        return nodes[nid.get()];
      }
      inline std::optional<Node> const& ato(NodeId const nid) const noexcept {
        assert(nid.get() < nodes.size());
        return nodes[nid.get()];
      }
      inline std::optional<Edge>& ato(EdgeId const eid) noexcept {
        assert(eid.get() < edges.size());
        return edges[eid.get()];
      }
      inline std::optional<Edge> const& ato(EdgeId const eid) const noexcept {
        assert(eid.get() < edges.size());
        return edges[eid.get()];
      }

      inline Node& at(NodeId const nid) noexcept {
        assert(nid.get() < nodes.size());
        assert(nodes[nid.get()]);
        return *nodes[nid.get()];
      }
      inline Node const& at(NodeId const nid) const noexcept {
        assert(nid.get() < nodes.size());
        assert(nodes[nid.get()]);
        return *nodes[nid.get()];
      }
      inline Edge& at(EdgeId const eid) noexcept {
        assert(eid.get() < edges.size());
        assert(edges[eid.get()]);
        return *edges[eid.get()];
      }
      inline Edge const& at(EdgeId const eid) const noexcept {
        assert(eid.get() < edges.size());
        assert(edges[eid.get()]);
        return *edges[eid.get()];
      }

      /* Edge methods */
      NodeId follow(EdgeId eid, NodeId from) const {
        Edge const& e = at(eid);
        if (e.nodes[0] == from) {
          return e.nodes[1];
        } else if (e.nodes[1] == from) {
          return e.nodes[0];
        } else {
          assert(0 && "invalid edge");
        }
      }
      /* end of Edge methods */

    private:

      /* Node methods */
      EdgeId test(NodeId from, NodeId neighbour) const {
        if (ato(from) && ato(neighbour)) {
          for (EdgeId const eid: at(from).adj) {
            if (follow(eid,from) == neighbour) {
              return eid;
            }
          }
        }
        return invalidE;
      }
      void directionalRemove(NodeId from, NodeId neighbour) {
        if (EdgeId const eid = test(from,neighbour)) {
          auto& adj = at(from).adj;
          for (EdgeId& eid2: adj) {
            if (eid == eid2) {
              eid2 = adj.back();
              adj.pop_back();
              break;
            }
          }
        }
      }
      /* end of Node methods */

      NodeId nodeIdx(Point pt) const {
        auto const lambda = [this,pt](auto const& cand) {
          return (!!cand) && this->geom.eq(pt,cand->point);
        };
        if (auto const it = std::find_if(nodes.begin(),nodes.end(),lambda); it != nodes.end()) {
          NodeId const nid(static_cast<std::size_t>(std::distance(nodes.begin(),it)));
          at(nid);
          return nid;
        }
        return invalidN;
      }
      template <typename T>
      static std::size_t mkGap(std::vector<std::optional<T>>& set) {
        auto const lambda = [](auto const& cand) {
          return !cand;
        };
        if (auto const it = std::find_if(std::next(set.begin()),set.end(),lambda); it != set.end()) {
          return static_cast<std::size_t>(std::distance(set.begin(),it));
        } else {
          std::size_t const id = set.size();
          set.push_back(std::nullopt);
          return id;
        }
      }

      Geom const& geom;
    public:
      Graph(Geom const& geom)
      : nodes(1)
      , edges(1)
      , geom(geom)
      { }
      Graph(Graph const&) = delete;
      Graph& operator=(Graph const&) = delete;
      Graph(Graph&&) = default;
      Graph& operator=(Graph&&) = delete;

      NodeId insert(Point const pt) {
        if (NodeId nid = nodeIdx(pt)) {
          return nid;
        } else {
          nid = NodeId(mkGap(nodes));
          ato(nid).emplace(nid,pt,enumResetAll<NTp>());
          return nid;
        }
      }

      bool remove(NodeId const nid) {
        if (nid != invalidN && !!ato(nid)) {
          { // remove all edges
            for (auto const eid: at(nid).adj) {
              directionalRemove(follow(eid,nid),nid);
              ato(eid) = std::nullopt;
            }
            at(nid).adj.clear();
          }
          ato(nid) = std::nullopt;
          return true;
        }
        return false;
      }
      bool remove(Node const& n) {
        return remove(n.id);
      }
      bool remove(Point const pt) {
        return remove(nodeIdx(pt));
      }

      EdgeId insert(PEdge const edge) {
        if (std::array<NodeId,2> const e = {nodeIdx(edge[0]),nodeIdx(edge[1])}; e[0] && e[1] && (e[0] != e[1]) ) {
          auto const eid0 = test(e[0],e[1]);
          auto const eid1 = test(e[1],e[0]);
          assert(eid0 == eid1);
          if (eid0 != invalidE) {
            return eid0;
          } else {
            EdgeId const eid = EdgeId(mkGap(edges));
            ato(eid).emplace(eid,e[0],e[1],edge,enumResetAll<ETp>());
            at(e[0]).adj.push_back(eid);
            at(e[1]).adj.push_back(eid);
            return eid;
          }
        }
        return invalidE;
      }

      bool remove(std::array<NodeId,2> const e) {
        if (e[0] != invalidN && e[1] != invalidN && e[0] != e[1]) {
          std::size_t removals = 0;
          std::array<EdgeId,2> eid{invalidE,invalidE};
          for (std::size_t k = 0; k < 2; ++k) {
            auto& adj = at(e[k]).adj;
            for (std::size_t i = 0; i < adj.size(); ++i) {
              if (follow(adj[i],e[k]) == e[1-k]) {
                eid[k] = adj[i];
                adj[i] = adj.back();
                adj.pop_back();
                ++removals;
                break;
              }
            }
          }
          assert(eid[0] == eid[1]);
          ato(eid[0]) = std::nullopt;
          (void)removals;
          assert((removals%2) == 0);
          return true;
        }
        return false;
      }
      bool remove(Edge const& e) {
        return remove(e.nodes);
      }
      bool remove(EdgeId const eid) {
        if (eid != invalidE && ato(eid)) {
          return removeEdge(at(eid));
        }
        return false;
      }
      bool remove(PEdge const edge) {
        return remove({nodeIdx(edge[0]),nodeIdx(edge[1])});
      }

      NTp& operator[](Point const pt) {
        if (auto const nid = insert(pt)) {
          return at(nid).flags;
        }
        assert(0 && "invalid point access");
        static NTp dummyflags = enumResetAll<NTp>();
        return dummyflags;
      }
      ETp& operator[](PEdge const edge) {
        if (auto const eid = insert(edge)) {
          return at(eid).flags;
        }
        assert(0 && "invalid edge access");
        static ETp dummyflags = enumResetAll<ETp>();
        return dummyflags;
      }

      Edge& edge(EdgeId const eid) {
        return at(eid);
      }
      Edge const& edge(EdgeId const eid) const {
        return at(eid);
      }

      template <typename C, bool Idx = false>
      class AccessWrapper {
        friend Graph;
        private:
          C* vec;
          AccessWrapper(C* vec) : vec(vec) {}
        public:
          class Iterator {
            friend AccessWrapper;
            private:
              C* vec;
              std::size_t i;
              Iterator(C* vec, std::size_t i)
                : vec(vec)
                , i(i) {}
            public:
              Iterator()
                : vec(nullptr)
                , i(0) {}
              Iterator(Iterator const&) = default;
              Iterator(Iterator&&) = default;
              Iterator& operator=(Iterator const&) = default;
              Iterator& operator=(Iterator&&) = default;

              auto& operator*() const {
                if constexpr (Idx) {
                  return i;
                } else {
                  return *(*vec)[i];
                }
              }
              Iterator& operator++() {
                do {
                  ++i;
                } while (i < vec->size() && !(*vec)[i]);
                return *this;
              }
              Iterator operator++(int) const {
                Iterator const copy = *this;
                ++*this;
                return copy;
              }
              friend bool operator==(Iterator const& l, Iterator const& r) {
                return (l.vec == r.vec) && (l.i == r.i);
              }
              friend bool operator!=(Iterator const& l, Iterator const& r) {
                return !(l == r);
              }
          };
          auto begin() const {
            Iterator b(vec,0);
            ++b;
            return b;
          }
          auto end() const {
            return Iterator(vec,vec->size());
          }
      };
      auto getNodes() const {
        return AccessWrapper<std::vector<std::optional<Node>> const>(&nodes);
      }
      auto getNodes() {
        return AccessWrapper<std::vector<std::optional<Node>>>(&nodes);
      }

      auto getEdges() const {
        return AccessWrapper<std::vector<std::optional<Edge>> const>(&edges);
      }
      auto getEdges() {
        return AccessWrapper<std::vector<std::optional<Edge>>>(&edges);
      }

      template <std::size_t N>
      std::vector<EdgeId> incidences(Edge const& from, std::array<std::size_t,N> sides = {0,1}) const {
        std::vector<EdgeId> result;
        using Bool = std::size_t;
        std::vector<Bool> seen(edges.size(),false);
        seen[from.id.get()] = true;
        for (std::size_t k: sides) {
          if (auto const nid = from.nodes[k]) {
            for (auto const& link: at(nid).adj) {
              if (!seen[link.get()]) {
                seen[link.get()] = true;
                result.push_back(link);
              }
            }
          }
        }
        return result;
      }

      std::vector<EdgeId> incidences(Edge const& from, NodeId const nid) const {
        for (std::size_t k: {0,1}) {
          if (nid == from.nodes[k]) {
            return incidences(from, std::array{k});
          }
        }
        return incidences(from, std::array<std::size_t,0>{});
      }

      template <std::size_t N>
      std::vector<PEdge> incidences(PEdge const from, std::array<std::size_t,N> sides = {0,1}) const {
        if (EdgeId const eid = test(nodeIdx(from[0]),nodeIdx(from[1]))) {
          auto const es = incidences(at(eid), sides);
          std::vector<PEdge> result;
          result.reserve(es.size());
          std::transform(es.begin(),es.end(),std::back_inserter(result),[this](EdgeId const& eid) {
            return this->edge(eid).pedge;
          });
          return result;
        }
        return {};
      }

      std::vector<Point> findCycle(Point const from) const {
        if (NodeId const n0 = nodeIdx(from)) {
          using Bool = std::size_t;
          std::vector<Bool> seen(edges.size(),0);
          seen[0] = true; // sentinel
          std::vector<std::tuple<NodeId,std::size_t,EdgeId>> stack{{n0,0,invalidE}};
          while (!stack.empty()) {
            // safety net, should literally never happen
            //if (stack.size() > (1ul<<20)) return{};
            assert(stack.size() < (1ul<<20));

            auto& [v,i,e] = stack.back();
            assert(seen[e.get()]);
            if (i >= at(v).adj.size()) {
              seen[e.get()] = false;
              stack.pop_back();
            } else {
              auto const eid = at(v).adj[i++];
              if (seen[eid.get()]) {
                // we already used that edge
                continue;
              }
              auto const w = follow(eid,v);
              if (w == n0) {
                // success, return the contents of the stack
                std::vector<Point> result;
                result.reserve(stack.size());
                for (auto const& [nid,i2,eid2]: stack) {
                  result.push_back(at(nid).point);
                }
                return result;
              } else {
                stack.emplace_back(w,0,eid);
                assert(!seen[eid.get()]);
                seen[eid.get()] = true;
              }
            }
          }
        }
        return {};
      }



  };
}
