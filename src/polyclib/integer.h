#pragma once

/*
 *  polyclib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <type_traits>
#include <ostream>

namespace polyclib {
  namespace util {
    template <typename Int, typename Tag>
    class SafeInt {
      Int value;
      static_assert(std::is_integral_v<Int>);
      public:
        constexpr SafeInt() noexcept : value() {}
        constexpr explicit SafeInt(Int const value) noexcept : value(value) {}
        SafeInt(SafeInt const&) = default;
        SafeInt(SafeInt&&) = default;
        SafeInt& operator=(SafeInt const&) = default;
        SafeInt& operator=(SafeInt&&) = default;
        constexpr Int get() const noexcept {
          return value;
        }

        explicit constexpr operator bool() const noexcept {
          return static_cast<bool>(value);
        }

        #define POLYCLIB_SAFEINT_MAKE_BIN(TP,OP) \
        constexpr inline friend TP operator OP (SafeInt const l, SafeInt const r) noexcept { \
          return TP(l.value OP r.value); \
        }
        POLYCLIB_SAFEINT_MAKE_BIN(bool,!=);
        POLYCLIB_SAFEINT_MAKE_BIN(bool,==);
        POLYCLIB_SAFEINT_MAKE_BIN(bool,<=); POLYCLIB_SAFEINT_MAKE_BIN(bool,<);
        POLYCLIB_SAFEINT_MAKE_BIN(bool,>=); POLYCLIB_SAFEINT_MAKE_BIN(bool,>);
        POLYCLIB_SAFEINT_MAKE_BIN(bool,&&); POLYCLIB_SAFEINT_MAKE_BIN(bool,||);
        POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,+);  POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,-);
        POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,*);  POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,/);
        POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,^);  POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,%);
        POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,&);  POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,|);
        POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,<<); POLYCLIB_SAFEINT_MAKE_BIN(SafeInt,>>);
        #undef POLYCLIB_SAFEINT_MAKE_BIN
        #define POLYCLIB_SAFEINT_MAKE_ASSIGN(OP) \
        constexpr inline friend SafeInt& operator OP (SafeInt& l, SafeInt const r) noexcept { \
          l.value OP r.value; \
          return l; \
        }
        POLYCLIB_SAFEINT_MAKE_ASSIGN(+=);  POLYCLIB_SAFEINT_MAKE_ASSIGN(-=);
        POLYCLIB_SAFEINT_MAKE_ASSIGN(*=);  POLYCLIB_SAFEINT_MAKE_ASSIGN(/=);
        POLYCLIB_SAFEINT_MAKE_ASSIGN(^=);  POLYCLIB_SAFEINT_MAKE_ASSIGN(%=);
        POLYCLIB_SAFEINT_MAKE_ASSIGN(&=);  POLYCLIB_SAFEINT_MAKE_ASSIGN(|=);
        POLYCLIB_SAFEINT_MAKE_ASSIGN(<<=); POLYCLIB_SAFEINT_MAKE_ASSIGN(>>=);
        #undef POLYCLIB_SAFEINT_MAKE_ASSIGN
        #define POLYCLIB_SAFEINT_MAKE_PRE(OP) \
        constexpr inline SafeInt& operator OP() noexcept { \
          OP value; \
          return *this; \
        }
        #define POLYCLIB_SAFEINT_MAKE_POST(OP) \
        constexpr inline SafeInt operator OP(int) noexcept { \
          SafeInt const copy = *this; \
          value OP; \
          return copy; \
        }
        POLYCLIB_SAFEINT_MAKE_PRE(++);  POLYCLIB_SAFEINT_MAKE_PRE(--);
        POLYCLIB_SAFEINT_MAKE_POST(++); POLYCLIB_SAFEINT_MAKE_POST(--);
        #undef POLYCLIB_SAFEINT_MAKE_PRE
        #undef POLYCLIB_SAFEINT_MAKE_POST
        #define POLYCLIB_SAFEINT_MAKE_UNARY(OP) \
        constexpr inline SafeInt operator OP() const noexcept { \
          return SafeInt(OP value); \
        }
        POLYCLIB_SAFEINT_MAKE_UNARY(+);  POLYCLIB_SAFEINT_MAKE_UNARY(-);
        POLYCLIB_SAFEINT_MAKE_UNARY(!);  POLYCLIB_SAFEINT_MAKE_UNARY(~);
        #undef POLYCLIB_SAFEINT_MAKE_UNARY

        inline friend std::ostream& operator<<(std::ostream& os, SafeInt const r) noexcept {
          os << r.value;
          return os;
        }
    };
  }
}
