#pragma once

/*
 *  polyclib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <optional>
#include <array>
#include <tuple>
#include <type_traits>

namespace polyclib {
  namespace util {

    namespace traits {
      template <typename T>
      constexpr T& mkRef();

      template <typename Point>
      constexpr auto gettp(typename Point::type*) { return static_cast<std::decay_t<typename Point::type>*>(nullptr); }
      template <typename Point>
      constexpr auto gettp(...                  ) { return static_cast<std::decay_t<decltype(mkRef<Point>()[0])>*>(nullptr); }

      template <typename Point>
      using Length = std::decay_t<decltype(*gettp<Point>(nullptr))>;
    }

    enum class Location {
      inside, outside, corner
    };

    template <typename P, typename L = traits::Length<P>>
    class GeomCalc {
      public:
        using Point = P;
        using Length = L;
        using Scalar = L;
        using Edge = std::array<Point,2>;

        Length const epsilon;


        static auto normal(Point x) {
          return Point{x[1],-x[0]};
        }

        static auto abs(Length len) {
          if (len <= -0) {
            return -len;
          }
          return len;
        }

        static Point sub(Point a) {
          a[0] = -a[0];
          a[1] = -a[1];
          return a;
        }
        static Point sub(Point a, Point b) {
          return add(a,sub(b));
        }
        static Point dir(Edge e) {
          return sub(e[1],e[0]);
        }

        static Point add(Point a, Point b) {
          a[0] += b[0];
          a[1] += b[1];
          return a;
        }

        static Point mul(Point a, Length b) {
          a[0] *= b;
          a[1] *= b;
          return a;
        }
        static Point mul(Length b, Point a) {
          return mul(a,b);
        }
        static Length mul(Point a, Point b) {
          return a[0]*b[0] + a[1]*b[1];
        }

        static auto sqlength(Point x) {
          return x[0]*x[0] + x[1]*x[1];
        }

        static auto sqlength(Point s, Point t) {
          auto const x = sub(t,s);
          return x[0]*x[0] + x[1]*x[1];
        }

        static auto sqlength(Edge e) {
          return sqlength(e[0],e[1]);
        }

        bool eq(Length a, Length b) const {
          return abs(a-b) < epsilon;
        }

        bool eq(Point a, Point b) const {
          return eq(a[0],b[0]) && eq(a[1],b[1]);
        }

        bool in(Length a, std::array<Length,2> bounds) const {
          return bounds[0] <= a && a <= bounds[1];
        }

        bool in_eps(Length a, std::array<Length,2> bounds) const {
          return (bounds[0]-epsilon) <= a && a <= (bounds[1]+epsilon);
        }

        Point at(Edge e, Scalar lambda) const {
          return add(e[0],mul(lambda,sub(e[1],e[0])));
        }

        bool on(Edge e, Point pt) const noexcept {
          return abs((e[1][0]-e[0][0])*(e[0][1]-pt[1]) - (e[0][0]-pt[0])*(e[1][1]-e[0][1])) < epsilon;
        }
        std::optional<Scalar> lambda(Edge e, Point pt) const noexcept {
          if (on(e,pt)) {
            auto const d = sub(e[1],e[0]);
            if (auto const cut = intersect_at(e,Edge{pt,add(pt,normal(d))})) {
              return (*cut)[0];
            }
          }
          return std::nullopt;
        }
        // compes the relative direction of b1-anchor relative to b0-anchor
        // returns {-1,0,1}, 0 indicates parallel or anti-parallel
        int side(Point anchor, Point b0, Point b1) const noexcept {
          auto const d0 = sub(b0,anchor);
          auto const d1 = sub(b1,anchor);
          auto const dn = normal(d1);
          auto const sign = mul(d0,dn);
          if (eq(sign,0)) {
            return 0;
          } else if (sign >= 0) {
            return 1;
          } else if (sign <= 0) {
            return -1;
          } else {
            return 0; // nans, infs, etc. not applicable here but gcc
          }
        }


        GeomCalc(Length epsilon) : epsilon(epsilon) {}

        std::optional<Length> non_parallel(Point s0, Point t0, Point s1, Point t1) const noexcept {
          if (sqlength(s0,t0) <= epsilon || sqlength(s1,t1) <= epsilon)
            return std::nullopt;
          Length const divisor = (s0[0]-t0[0])*(s1[1]-t1[1])-(s0[1]-t0[1])*(s1[0]-t1[0]);
          if (abs(divisor) <= epsilon) {
            return std::nullopt;
          }
          return divisor;
        }
        auto non_parallel(Edge e0, Edge e1) const noexcept {
          return non_parallel(e0[0],e0[1],e1[0],e1[1]);
        }

        auto parallel(Edge e0, Edge e1) const noexcept {
          return !non_parallel(e0,e1);
        }


        std::optional<std::array<Scalar,2>> intersect_at(Point s0, Point t0, Point s1, Point t1) const noexcept {
          if (auto const npl = non_parallel(s0,t0,s1,t1)) {
            auto const divisor = *npl;
            Scalar const a0 = ((s0[0]-s1[0])*(s1[1]-t1[1])-(s0[1]-s1[1])*(s1[0]-t1[0])) / divisor;
            Scalar const a1 = ((t0[0]-s0[0])*(s0[1]-s1[1])-(t0[1]-s0[1])*(s0[0]-s1[0])) / divisor;
            return std::array<Scalar,2>{a0,a1};
          }
          return std::nullopt;
        }
        std::optional<std::array<Scalar,2>> intersect_at(Edge e0, Edge e1) const noexcept {
          return intersect_at(e0[0],e0[1],e1[0],e1[1]);
        }

        std::optional<std::tuple<Point,Location,Scalar,Scalar>> intersect(Edge e0, Edge e1) const noexcept {
          if (auto const op = intersect_at(e0,e1)) {
            auto const [a0,a1] = *op;
            Location loc = Location::outside;
            if (in_eps(a0,{0,1}) && in_eps(a1,{0,1})) {
              if (eq(a0,0) || eq(a1,0) || eq(a0,1) || eq(a1,1)) {
                loc = Location::corner;
              } else {
                loc = Location::inside;
              }
            }
            return std::make_tuple(add(e0[0],mul(a0,sub(e0[1],e0[0]))),loc,a0,a1);
          }
          return std::nullopt;
        }

        std::optional<std::array<Scalar,2>> overlap(Edge e0, Edge e1) const noexcept {
          if (parallel(e0,e1)) {
            if (on(e0,e1[0]) && on(e0,e1[1]) && on(e1,e0[0]) && on(e1,e0[1])) {
              auto const d = sub(e1[1],e1[0]);
              auto const cut0 = intersect_at(e0,Edge{e1[0],add(e1[0],normal(d))});
              auto const cut1 = intersect_at(e0,Edge{e1[1],add(e1[1],normal(d))});
              if (cut0 && cut1) {
                Scalar a0 = (*cut0)[0];
                Scalar a1 = (*cut1)[0];
                if (a1 < a0) {
                  std::swap(a0,a1);
                }
                a0 = std::max<Length>(0,a0);
                a1 = std::min<Length>(1,a1);
                if (a1 <= a0) return std::nullopt;
                return std::array<Scalar,2>{a0,a1};
              } // else: something went extremely wrong
            }
          }
          return std::nullopt;
        }
    };
  }
}
