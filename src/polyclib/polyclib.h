#pragma once

/*
 *  polyclib
 *  Copyright (C) 2020  bitzoid
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "geom.h"
#include "graph.h"

#include <cstdint>
#include <array>
#include <vector>
#include <initializer_list>
#include <algorithm>
#include <optional>

namespace polyclib {


  template <typename Point, typename Length = util::traits::Length<Point>>
  class Clipper {
    public:
      using GeomCalc = util::GeomCalc<Point,Length>;
      using Edge = typename GeomCalc::Edge;
      using Scalar = typename GeomCalc::Scalar;
      using Polygon = std::vector<Point>;
      using G = Graph<GeomCalc>;
      using NTp = typename G::NTp;
      using ETp = typename G::ETp;
    private:
      GeomCalc geom;
      Polygon subject;
      std::vector<Polygon> clips;

      static Edge edge(std::size_t i, Polygon const& pl) noexcept {
        return Edge{pl[i], pl[(i+1)%pl.size()]};
      }

      using EdgeFrag = std::tuple<Point,NTp,ETp>;

      // chopps up an edge along all intersections of a Polygon
      // returns a point segment to replace the current edge
      std::vector<EdgeFrag> edgeFragments(std::array<EdgeFrag,2> const& ef, Polygon const& clip, NTp where, ETp edgeType) const {
        std::vector<EdgeFrag> pts;
        pts.reserve(4);
        pts.emplace_back(ef[0]);
        Edge const e{std::get<0>(ef[0]),std::get<0>(ef[1])};
        NTp cutflag = NTp::cut;
        NTp cornerflag = NTp::corner;
        NTp clipflag = NTp::clip;
        enumSet(cutflag,NTp::clip);
        enumSet(cornerflag,NTp::clip);
        enumSet(cutflag,where);
        enumSet(cornerflag,where);
        enumSet(clipflag,where);
        ETp epflag = ETp::clip;
        enumSet(epflag,edgeType);
        std::vector<Edge> eps;
        for (std::size_t j = 0; j < clip.size(); ++j) {
          Edge const cross = edge(j,clip);
          if (auto pt = geom.intersect(e,cross)) {
            std::optional<Point> joint;
            switch (std::get<1>(*pt)) {
              case util::Location::inside: {
                pts.emplace_back(std::get<0>(*pt),cutflag,edgeType);
              } break;
              case util::Location::corner: {
                pts.emplace_back(std::get<0>(*pt),cornerflag,edgeType);
              } break;
              case util::Location::outside:
              default: {}
            }
          } else if (auto const cuts = geom.overlap(e,edge(j,clip))) {
            Edge const ep{geom.at(e,(*cuts)[0]),geom.at(e,(*cuts)[1])};
            pts.emplace_back(e[0],clipflag,edgeType);
            pts.emplace_back(e[1],clipflag,edgeType);
            eps.push_back(ep);
          }
        }
        pts.emplace_back(ef[1]);
        std::sort(pts.begin(),pts.end(),[this,start=std::get<0>(pts.front())](auto l, auto r) {
          return
            this->geom.sqlength(std::get<0>(l),start) <
            this->geom.sqlength(std::get<0>(r),start);
        });
        for (auto const& ep: eps) {
          std::size_t k = 0;
          while (k < pts.size() && !geom.eq(ep[0],std::get<0>(pts[k]))) {
            ++k;
          }
          while (k < pts.size() && !geom.eq(ep[1],std::get<0>(pts[k]))) {
            ++k;
            if (k < pts.size()) {
              enumSet(std::get<2>(pts[k]),epflag);
            }
          }
        }
        return pts;
      }

      void cut(G& graph, Edge const& e, NTp ntp, ETp etp) const {
        std::vector<EdgeFrag> fragments{
          EdgeFrag{e[0],ntp,etp},
          EdgeFrag{e[1],ntp,etp}
        };
        for (auto const& clip: clips) {
          assert(!fragments.empty());
          std::vector<EdgeFrag> nfragments{fragments.front()};
          for (std::size_t i = 0; i+1 < fragments.size(); ++i) {
            auto const efs = edgeFragments({fragments[i],fragments[i+1]}, clip, ntp, etp);
            assert(efs.size() >= 2);
            std::copy(std::next(efs.begin()),efs.end(),std::back_inserter(nfragments));
          }
          fragments = nfragments;
        }
        for (std::size_t i = 0; i+1 < fragments.size(); ++i) {
          Edge const candidate{std::get<0>(fragments[i]),std::get<0>(fragments[i+1])};
          if (!geom.eq(candidate[0],candidate[1])) {
            enumSet(graph[candidate[0]], std::get<1>(fragments[i]));
            enumSet(graph[candidate[1]], std::get<1>(fragments[i+1]));
            enumSet(graph[Edge{candidate[0],candidate[1]}],std::get<2>(fragments[i+1]));
          }
        }
      }

      void cut(G& graph, Polygon const& pg, NTp ntp, ETp etp) const {
        for (std::size_t i = 0; i < pg.size(); ++i) {
          cut(graph,edge(i,pg),ntp,etp);
        }
      }


      std::array<Point,2> boundingBox(Polygon const& pg) const noexcept {
        std::array<Point,2> bb{pg[0],pg[1]};
        for (auto const& pt: pg) {
          for (std::size_t i = 0; i < 2; ++i) {
            bb[0][i] = std::min(bb[0][i],pt[i]);
            bb[1][i] = std::max(bb[1][i],pt[i]);
          }
        }
        return bb;
      }
      Point outsideChoose(Polygon const& pg) const noexcept {
        auto const bb = boundingBox(pg);
        auto const width = bb[1][0]-bb[0][0];
        auto const height = bb[1][1]-bb[0][1];
        Point pt = bb[0];
        Scalar const perturbX = static_cast<Scalar>(3.1415926535897932384626433/2);
        Scalar const perturbY = static_cast<Scalar>(2.7182818284590452353602874/2);
        pt[0] -= perturbX*width;
        pt[1] -= perturbY*height;
        return pt;
      }
      auto raycast(Polygon const& pg, Edge ray) const noexcept {
        std::vector<std::tuple<Edge,Point,util::Location,Scalar>> result;
        std::vector<std::tuple<Edge,Point,util::Location,Scalar>> corners;
        // FIXME what about parallel intersections!
        for (std::size_t i = 0; i < pg.size(); ++i) {
          Edge const cross = edge(i,pg);
          if (auto const is = geom.intersect(ray,cross)) {
            auto const& [pt, loc, a0, a1] = *is;
            if (loc == util::Location::inside) {
              result.emplace_back(cross,pt,loc,a0);
            }
            if (loc == util::Location::corner) {
              corners.emplace_back(cross,pt,loc,a0);
            }
          }
        }
        // process corners
        while (!corners.empty()) {
          auto const sideOf = [this,ray](auto const& elem) {
            auto const& e = std::get<0>(elem);
            Point const anchor = std::get<1>(elem);
            Point const pt = (this->geom.eq(e[0],anchor))?e[1]:e[0];
            return this->geom.side(anchor,ray[1],pt);
          };
          auto const pivotElem = corners.back();
          Point const pivot = std::get<1>(pivotElem);
          //int const pivotSide = sideOf(pivot);
          std::copy_if(corners.begin(),corners.end(),std::back_inserter(result),
            [this,pivot,sideOf,pivotSide=sideOf(pivotElem)](auto elem) {
              return this->geom.eq(pivot,std::get<1>(elem))
                  && (sideOf(elem) == pivotSide);
            }
          );
          corners.erase(std::remove_if(corners.begin(),corners.end(),
            [this,pivot](auto const& elem) { return this->geom.eq(pivot,std::get<1>(elem)); }
          ),corners.end());
        }
        std::sort(result.begin(),result.end(),[](auto l, auto r) {
          return std::get<3>(l) < std::get<3>(r);
        });
        return result;
      }
      auto raycast(Polygon const& pg, Point target) const noexcept {
        return raycast(pg,Edge{outsideChoose(pg),target});
      }
      bool insideCheck(Polygon const& pg, Point pt, bool borderInside) const noexcept {
        auto const ray = raycast(pg,pt);
        if (ray.size() > 0 && std::get<3>(ray.back()) >= 1-geom.epsilon) {
          return borderInside;
        }
        return !!(ray.size() % 2);
      }

    public:
      struct CutData {
        CutData(CutData const&) = delete;
        CutData(CutData&&) = default;
        CutData& operator=(CutData const&) = delete;
        CutData& operator=(CutData&&) = delete;

        G graph;

        CutData(GeomCalc const& geom) : graph(geom) {}

        std::optional<Polygon> extract(ETp what) {
          for (auto const& e: graph.getEdges()) {
            if (enumTest(e.flags,what)) {
              auto const c = graph.findCycle(e.pedge[0]);
              if (!c.empty()) {
                for (auto const pt: c) {
                  graph.remove(pt);
                }
                return c;
              }
              assert(enumTest(e.flags,ETp::clip));
            }
          }
          return std::nullopt;
        }
        std::optional<Polygon> extract(bool what = true) {
          if (what) {
            return extract(ETp::subject);
          } else {
            return extract(ETp::clip);
          }
        }
      };

    private:
      void addBypasses(CutData& cd) const {
        for (auto const& clip: clips) {
          for (std::size_t i = 0; i < clip.size(); ++i) {
            auto const e = edge(i,clip);
            std::vector<std::tuple<Scalar,Point>> joints;
            for (auto const& n: cd.graph.getNodes()) {
              if (auto const lambda = geom.lambda(e,n.point); lambda && -geom.epsilon <= *lambda && *lambda <= 1+geom.epsilon) {
                joints.emplace_back(*lambda,n.point);
              }
            }
            std::sort(joints.begin(),joints.end(),[](auto const& j0, auto const& j1) {
              return std::get<0>(j0) < std::get<0>(j1);
            });
            for (std::size_t k = 0; k+1 < joints.size(); ++k) {
              Edge const e {std::get<1>(joints[k]),std::get<1>(joints[k+1])};
              cut(cd.graph,e,NTp::clip,ETp::clip);
            }
          }
        }
      }

      void dropOutsiders(CutData& cd) const {
        for (auto const& n: cd.graph.getNodes()) {
          if (!insideCheck(subject,n.point,true)) {
            cd.graph.remove(n);
          }
        }
      }

      void dropObstructed(CutData& cd) const {
        auto const& its = cd.graph.getEdges();
        for (auto const& e: its) {
          auto const mid = geom.at(e.pedge,0.5);
          bool drop = false;
          for (auto const& clip: clips) {
            //auto const out = outsideChoose(clip);
            //cd.graph[out];
            //cd.graph[mid];
            //cd.graph[Edge{out,mid}];
            if (insideCheck(clip,mid,false)) {
              drop = drop || true;
              break;
            }
          }
          if (drop) {
            cd.graph.remove(e);
          }
        }
      }

    private:
      [[deprecated]]
      bool followSuperEdgeInside(G const& graph, typename G::Edge const& e, typename G::NodeId const nid, typename G::NodeId const stopAt) const {
        if (nid == stopAt) {
          return true;
        }
        for (auto fid: graph.incidences(e,nid)) {
          auto const& f = graph.edge(fid);
          if (enumTest(f.flags,ETp::clip)) {
            auto const mid = geom.at(f.pedge,0.5);
            if (insideCheck(subject,mid,false)) {
              return true;
            } else if (enumTest(f.flags,ETp::subject)) {
              // super edge, allowed to follow
              if (followSuperEdgeInside(graph, f, graph.follow(fid, nid), stopAt)) {
                return true;
              }
            }
          }
        }
        return false;
      }

      bool isSafeSuperEdge(G const& graph [[maybe_unused]], typename G::Edge const& e) const {
        if (enumTest(e.flags,ETp::subject) && enumTest(e.flags,ETp::clip)) {
          auto const mid = geom.at(e.pedge,0.5);
          auto nrm = geom.normal(geom.dir(e.pedge));
          auto const lim = geom.epsilon*100;
          while (geom.sqlength(nrm) > lim*lim) {
            nrm = geom.mul(nrm, 0.5);
          }
          for (auto side: {geom.add(mid,nrm),geom.sub(mid,nrm)}) {
            if (insideCheck(subject, side, false)) {
              for (auto const& clip: clips) {
                if (insideCheck(clip, side, false)) {
                  return false;
                }
              }
            }
          }
        }
        return true;
      }

    public:
      void dropFalseSuperEdges(CutData& cd) const {
        for (auto const& e: cd.graph.getEdges()) {
          if (!isSafeSuperEdge(cd.graph, e)) {
            cd.graph.remove(e);
          }
        }
        // remove identities
        for (auto const& clip: clips) {
          std::size_t cnt = 0;
          for (auto const& pt: subject) {
            if (insideCheck(clip,pt,true)) {
              ++cnt;
            }
          }
          if (cnt == subject.size()) {
            // the subject is fully contained in clip
            for (auto const& pt: subject) {
              cd.graph.remove(pt);
            }
          }
        }
      }




    public:
      Clipper(Length epsilon) : geom(epsilon), subject() {}
      template <typename P, typename = std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<P>>, Polygon>>>
      Clipper(P&& subj, Length epsilon) : geom(epsilon), subject(std::forward<P>(subj)) {}

      template <typename P, typename = std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<P>>, Polygon>>>
      void resetSubject(P&& subj) {
        subject = std::forward<P>(subj);
      }

      template <typename P, typename = std::enable_if_t<std::is_same_v<std::remove_cv_t<std::remove_reference_t<P>>, Polygon>>>
      void clipBy(P&& poly) {
        clips.emplace_back(std::forward<P>(poly));
      }

      void clearClips() {
        clips.clear();
      }


      CutData constructCut() const {
        CutData cd(geom);
        auto& graph = cd.graph;
        for (auto v: subject) {
          graph[v] = NTp::subject;
        }
        for (auto clip: clips) {
          for (auto v: clip) {
            graph[v] = NTp::clip;
          }
        }
        cut(graph,subject,NTp::subject,ETp::subject);
        addBypasses(cd);
        dropOutsiders(cd);
        dropObstructed(cd);
        dropFalseSuperEdges(cd);
        return cd;
      }
  };
}
