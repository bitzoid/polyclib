# polyclib -- a flexible polygon clipping library for c++

This implements an algorithm that is very slightly related to Greiner-Hormann but can handle degeneracies and supports clipping with several clipping-polygons simultaneously.

It has worse complexity than Greier-Hormann for being more general.

## Example clipping

A clipping can be computed by instantiating a `polyclib::Clipper<Point>` object, where point is a suitable representation of a Point in 2D space. For instance `std::array<double,2>`. Notably, you can use a rational numbers library in place of doubles. In theory the algorithm could handle signed integers, but the geometry core currently does not.

An instantiation could look like this:

    #include "polyclib/polyclib.h"
    // ...
    using Coord = double;
    constexpr Coord epsilon = 1e-6; // choose something small but non-zero
    using Point = std::array<Coord,2>;
    using Polygon = std::vector<Point>;

    // the subject we want to clip
    Polygon subj = {{0,0},{0,100},{100,100},{100,0}};
    polyclib::Clipper<Point> clipper(subj,epsilon);

After constructing a clipper instance we can add clipping polygons to it:

    std::vector<Polygon> cls;
    cls.emplace_back(Polygon{{40,-15},{60,-15},{60,115},{40,115}});
    cls.emplace_back(Polygon{{20,-30},{50,-30},{80,130},{50,130}});
    // ...
    for (auto const& cl: cls) {
      clipper.clipBy(cl);
    }

That's it, the algorithm knows everything it needs to know. Run it:

    // this doesn't alter the state of clipper, a new cut could be run
    CutData cd = clipper.constructCut();

With the CutData object, we can list all the resulting polygons:

    for (std::optional<Polygon> oc; oc = cd.extract(true); ) {
      auto const& c = *oc;
      do_smth_with_polygon(c);
    }

Lastly, we extract all strictly contained clipping polygons.

    for (std::optional<Polygon> oc; oc = cd.extract(false); ) {
      auto const& c = *oc;
      do_smth_with_cutout(c);
    }

### Example image

#### Polygons

    cls.emplace_back(Polygon{{40,-15},{60,-15},{60,115},{40,115}});
    cls.emplace_back(Polygon{{20,-30},{50,-30},{80,130},{50,130}});
    cls.emplace_back(Polygon{{100,-20},{130,-20},{130,40},{100,40}});
    cls.emplace_back(Polygon{{0,0},{-20,0},{-20,-20},{0,-20}});
    cls.emplace_back(Polygon{{20,80},{-20,80},{-20,120},{20,120}});
    cls.emplace_back(Polygon{{80,100},{150,100},{80,70}});
    cls.emplace_back(Polygon{{0,40},{0,60},{30,95},{20,40}});
    cls.emplace_back(Polygon{{80,10},{93,17},{84,30}});

#### Result

![Before clipping](doc/example1.png)
![After clipping](doc/example2.png)


## Author

*  bitzoid {zoid at riseup dot net}
